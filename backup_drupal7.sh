#!/bin/bash

#
# Pour le moment, une simple sauvegarde de la base de données
#


PATH_HOME="drupal_7/"
PATH_CONFIG="${PATH_HOME}sites/default/"
DB_NAME7="site7"

# Dump du contenu de la base de données
echo "Dump du contenu de la base de données"
mysqldump -u root -proot ${DB_NAME7} > /tmp/dump_7.sql

# On met le fichier de côté
mv /tmp/dump_7.sql ./dump_7.sql

