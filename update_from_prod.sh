#!/bin/bash

#
# Récupération en local du site de production actuel
#


# Rapatriement de la dernière version du site de prod
echo "Synchronisation des fichiers du site"
rsync root@tau.lqdn.fr:/var/www/site/* drupal_6 -avzP --delete --exclude=files/* --exclude=pi-billion.txt

# Modification de la configuration pour ne pas bloquer la connexion des utilisateurs
sed -r 's#(\$conf\[.session_inc)#//\1#' drupal_6/sites/default/settings.php > settings.php
chmod 755 drupal_6/sites/default
chmod +w drupal_6/sites/default/settings.php
mv settings.php drupal_6/sites/default/settings.php
chmod -w drupal_6/sites/default/settings.php
chmod 555 drupal_6/sites/default

# Récupération des informations de connexion à la base de données
echo "Récupération des informations de connexions à la base de données"
DB_USER=`cat drupal_6/sites/default/settings.php |grep -e '^\$db_url' |sed -r 's/[^\/]+\/\/([^:]+).*/\1/'`
DB_PWD=`cat drupal_6/sites/default/settings.php |grep -e '^\$db_url' |sed -r 's/[^\/]+[^:]+:([^@]+).*/\1/'`
DB_NAME=`cat drupal_6/sites/default/settings.php |grep -e '^\$db_url' |sed -r "s/[^@]+[^\/]+\/([^']+).*/\1/"`
echo "${DB_NAME} / ${DB_PWD}"

# On fait un dump de la base de données à distance
echo "Récupération de la base de données"
ssh root@tau.lqdn.fr "mysqldump ${DB_NAME} > /tmp/dump.sql" 

# On supprime le fichier dump si présent 
if test -f dump.sql
then
    rm dump.sql
fi

# On rapatrie ce fichier
rsync -avzP root@tau.lqdn.fr:/tmp/dump.sql ./dump.sql

# On crée la base si nécessaire
mysql -u root -proot -e "create database if not exists ${DB_NAME}"

# On réintègre la base de prod
echo "Reconstruction de la base de données"
mysql -u root -proot ${DB_NAME} < dump.sql

# On regénère l'utilisateur et on lui donne accès
echo "Remise en place des accès à la base de données"
mysql -u root -proot -e "drop user '${DB_USER}'@'localhost'"
mysql -u root -proot -e "create user '${DB_USER}'@'localhost' identified by '${DB_PWD}'"
mysql -u root -proot -e "grant all privileges on ${DB_NAME}.* to '${DB_USER}'@'localhost'"

# On fait le ménage dans les tables
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.accesslog;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.cache;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.cache_apachesolr;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.cache_block;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.cache_filter;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.cache_form;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.cache_menu;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.cache_page;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.cache_update;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.cache_views;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.cache_views_data;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.ctools_css_cache;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.ctools_object_cache;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.search_dataset;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.search_index;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.search_node_links;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.search_total;"
mysql -u root -proot -e "TRUNCATE TABLE ${DB_NAME}.watchdog;"

