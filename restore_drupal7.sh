#!/bin/bash

#
# Pour le moment, on ne restaure que la base de données
#

PATH_HOME="drupal_7/"
PATH_CONFIG="${PATH_HOME}sites/default/"
DB_NAME7="site7"

# On écrase et remplace la base de données
echo "Restauration du contenu de la base de données"
mysql -u root -proot ${DB_NAME7} < dump_7.sql

