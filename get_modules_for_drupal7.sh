#!/bin/bash

PATH_HOME="drupal_7/"
PATH_CONFIG="${PATH_HOME}sites/default/"
DB_NAME7="site7"

# Rapatriement de la dernière version de drupal
echo "Téléchargement de Drupal 7"
wget http://ftp.drupal.org/files/projects/drupal-7.34.tar.gz

echo "Extraction de l'archive"
tar -xvf drupal-7.34.tar.gz

echo "Suppression de l'ancien dossier s'il existe"
if test -d drupal_7
then
    rm -rf drupal_7
fi

echo "Renommage du dossier"
mv drupal-7.34 drupal_7

echo "Préparation de la configuration"
`cp ${PATH_CONFIG}default.settings.php ${PATH_CONFIG}settings.php`

echo "Suppression de l'archive"
rm drupal-7.34.tar.gz

# Création du dossier files
`mkdir ${PATH_CONFIG}files`

# Mise en place des droits
`chmod o+w ${PATH_CONFIG}files`

# Récupération des informations de connexion à la base de données
echo "Récupération des informations de connexions à la base de données"
DB_USER=`cat drupal_6/sites/default/settings.php |grep -e '^\$db_url' |sed -r 's/[^\/]+\/\/([^:]+).*/\1/'`
DB_PWD=`cat drupal_6/sites/default/settings.php |grep -e '^\$db_url' |sed -r 's/[^\/]+[^:]+:([^@]+).*/\1/'`
DB_NAME=`cat drupal_6/sites/default/settings.php |grep -e '^\$db_url' |sed -r "s/[^@]+[^\/]+\/([^']+).*/\1/"`
echo "${DB_NAME} / ${DB_PWD}"

# On recrée la base
mysql -u root -proot -e "drop database if exists ${DB_NAME7}"
mysql -u root -proot -e "create database ${DB_NAME7}"

# On met en place les accès
echo "Mise en place des accès à la base de données"
mysql -u root -proot -e "grant all privileges on ${DB_NAME7}.* to '${DB_USER}'@'localhost'"

# Modification de la configuration pour se connecter à la base de données
DATABASE="\n
\$databases['default'] = array();\n
\n
// Main database\n
\$databases['default']['default'] = array(\n
 'driver' => 'mysql',\n
 'database' => '${DB_NAME7}',\n
 'username' => '${DB_USER}',\n
 'password' => '${DB_PWD}',\n
 'host' => 'localhost',\n
 'prefix' => '',\n
);\n
\n"
`echo -e ${DATABASE} >> ${PATH_CONFIG}settings.php`

